<?php
define('TPL_DIR_URI', get_template_directory_uri());

# Khai báo hằng số TPL_DIR bằng đường dẫn đến thư mục theme
define('TPL_DIR', get_stylesheet_directory());

# Yêu cầu load file class-wp-bootstrap-navwalker.php
function register_navwalker()
{
    require_once TPL_DIR . '/inc/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

function svh_enqueue_styles()
{
    wp_enqueue_style('bootstrap', TPL_DIR_URI . '/assets/vendor/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('core', TPL_DIR_URI . '/style.css');
    wp_enqueue_style('fonts', TPL_DIR_URI . '/assets/fonts/Poppins/stylesheet.css');
    wp_enqueue_style('slick', TPL_DIR_URI . '/assets/vendor/slick/slick.css');
    wp_enqueue_style('slick-theme', TPL_DIR_URI . '/assets/vendor/slick/slick-theme.css');
}
add_action('wp_enqueue_scripts', 'svh_enqueue_styles');

function svh_enqueue_scripts()
{
    wp_enqueue_script('popper', TPL_DIR_URI . '/assets/vendor/popper/popper.min.js', ['jquery']);
    wp_enqueue_script('bootstrap', TPL_DIR_URI . '/assets/vendor/bootstrap/js/bootstrap.min.js', ['jquery']);
}
add_action('wp_enqueue_scripts', 'svh_enqueue_scripts');

/**
  @ Them script vào hook footer
 **/
function svh_footer_enqueue_script()
{
    wp_enqueue_script('slick', TPL_DIR_URI . '/assets/vendor/slick/slick.min.js', ['jquery']);
    wp_enqueue_script('countTo', TPL_DIR_URI . '/assets/js/jquery.countTo.js', ['jquery']);
    wp_enqueue_script('main script', TPL_DIR_URI . '/assets/js/main.js', ['jquery']);
}
add_action('wp_footer', 'svh_footer_enqueue_script');
/**
  @ Xoá margin-top: 32px !important;
 **/
// function remove_admin_login_header()
// {
//     remove_action('wp_head', '_admin_bar_bump_cb');
// }
// add_action('get_header', 'remove_admin_login_header');

/**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
 **/
if (!function_exists('theme_feature_setup')) {
    /*
     * Nếu chưa có hàm theme_feature_setup() thì sẽ tạo mới hàm đó
     */
    function theme_feature_setup()
    {
        /*
        * Thiết lập theme có thể dịch được
        */
        $language_folder = TPL_DIR_URI . '/languages';
        load_theme_textdomain('vfftech-illdy', $language_folder);
        /*
        * Thêm chức năng post thumbnail
        */
        add_theme_support('post-thumbnails');
        /*
        * Thêm chức năng title-tag để tự thêm <title>
        */
        add_theme_support('title-tag');
        /*
        * Thêm chức năng custom background
        */
        $default_background = array(
            'default-color' => '#e8e8e8',
        );
        add_theme_support('custom-background', $default_background);

        /*
        * Tạo menu cho theme
        */
        register_nav_menus([
            'main-menu' => __('Top menu')
        ]);

        /*
        * Tạo sidebar cho theme
        */
        $sidebar = array(
            'name' => __('Main Sidebar', 'vfftech-illdy'),
            'id' => 'main-sidebar',
            'description' => 'Main sidebar for vfftech-illdy theme',
            'class' => 'main-sidebar',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebar);
    }
    add_action('init', 'theme_feature_setup');
}

if (!function_exists('display_menu')) {
    function display_menu($slug)
    {
        $menu = array(
            'theme_location' => $slug,
            'container' => 'div',
            'container_id' => 'myNavbar',
            'container_class' => 'collapse navbar-collapse',
            'menu_class'        => 'text-center nav navbar-nav navbar-right'
        );
        wp_nav_menu($menu);
    }
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'     => 'Theme options', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Theme options', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'    => false
    ));
}
