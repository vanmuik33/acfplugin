<?php get_header(); ?>
<h2>TIN TỨC</h2>
<?php 
if ( have_posts() ) : 
while ( have_posts() ) : 
the_post(); 
?>

    <article>
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <time><?php the_time('F jS, Y'); ?> </time>
        <p><?php the_excerpt(); ?></p>
        <p>Danh mục:<?php the_category( ', ' ); ?></p>
    </article>

 <?php 
 endwhile; 
 endif;
 ?>

<?php get_footer(); ?>