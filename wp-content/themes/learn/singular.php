<?php get_header(); ?>

    <article> 
        <h2><?php the_title(); ?></h2>
        <time><?php the_time('F jS, Y'); ?> </time>
        <p>
            <?php 
                    the_post();
                    the_content();
            ?>
        </p>
        <p>Danh mục:<?php the_category( ', ' ); ?></p>
    </article>

<?php get_footer(); ?>