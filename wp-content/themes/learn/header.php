<!DOCTYPE html>
<html lang="en">

<head>
    <title>Front Page - Illdy Theme Demo</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" />
    <meta charset="<?php bloginfo('charset'); ?>" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
    <!-- page loading  -->
    <div class="pace-overlay"></div>
    <!-- ===================================================================== header =====================================================================-->
    <header id="header" style="background-image: url(<?php echo get_field('banner_group')['background'] ?>);">
        <div class="top-header">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="<?php echo get_field('banner_group')['logo'] ?>" alt=""></a>
                    </div>
                    <?php display_menu('main-menu') ?>
                </div>
            </nav>
        </div>
        <div class="bottom-header text-center">
            <div class="container">
                <div class="row text-white">
                    <div class="col-sm-12">
                        <h1><?php
                            $banner = get_field('banner_group');
                            $title = str_replace('.', '<span style="color: ' . $banner['divider_color'] . ';">.</span>', $banner['title']);
                            echo $title;
                            ?>
                        </h1>

                    </div>
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="section-description" style="color: <?php echo get_field('banner_group')['description_color'] ?>">
                            <?php
                                echo get_field('banner_group')['description'];
                            ?>
                        </div>
                        <a href="<?php echo get_post_type_archive_link('post') ?>" title="Learn more" class="header-button-one">Learn more</a>
                        <a href="#" title="Download" class="header-button-two">Download</a>
                    </div>
                </div>
            </div>
        </div>
    </header>