<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="title">
                        <h3>PRODUCTS</h3>
                    </div>
                    <ul>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Our work</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Club</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>News</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Announcement</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="title">
                        <h3>INFORMATION</h3>
                    </div>
                    <ul>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Pricing</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Terms</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Affiliates</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Blog</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <div class="title">
                        <h3>SUPPORT</h3>
                    </div>
                    <ul>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Documentation</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>FAQs</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Forums</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="container">
                <p class="copyright">
                    <span>Theme: <a href="#" title="Illdy" target="_blank">Illdy</a>.</span>
                    <span class="bottom-copyright" data-customizer="copyright-credit">© Copyright 2016. All Rights Reserved.</span>
                </p>
            </div>
        </div>
    </footer>
	<?php wp_footer() ?>
</body>

</html>
