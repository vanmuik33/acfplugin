// jQuery(function() {
//     jQuery('.counter-number').countTo();
// })

var isInViewport = function (elem) {
  var bounding = elem.getBoundingClientRect();
  return (
    bounding.top >= 0 &&
    bounding.left >= 0 &&
    bounding.bottom <=
      (window.innerHeight || document.documentElement.clientHeight) &&
    bounding.right <=
      (window.innerWidth || document.documentElement.clientWidth)
  );
};

jQuery(window).on('load', function(event) {
    jQuery('body').removeClass('preloading');
});

jQuery(document).ready(function () {
  var pos = document.querySelector("#counter");
  window.addEventListener(
    "scroll",
    function (event) {
      if (isInViewport(pos)) {
        jQuery(function () {
          jQuery(".counter-number").addClass("number-show");
          jQuery(".counter-number").countTo();
        });
      }
    },
    false
  );
});

jQuery(document).ready(function () {
  jQuery(".slick-slider").slick({
    dots: true,
    autoplay: false,
    slidesToScroll: 1,
    accessibility: false,
    arrows: false,
    swipeToSlide: true,
  });
});
