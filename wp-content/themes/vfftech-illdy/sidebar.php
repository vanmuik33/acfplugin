<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package vfftech-illdy
 */

if ( ! is_active_sidebar( 'main-sidebar' ) ) {
	return;
}
?>

<div id="sidebar" class="widget-area">
	<?php dynamic_sidebar( 'main-sidebar' ); ?>
</div><!-- #secondary -->
