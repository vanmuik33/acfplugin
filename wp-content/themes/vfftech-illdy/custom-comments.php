<?php
function better_comments($comment, $args, $depth)
{
    // var_dump($args);
    if ('div' === $args['style']) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    } ?>
    <<?php echo $tag; ?> <?php comment_class(empty($args['has_children']) ? '' : 'parent'); ?> id="li-comment-<?php comment_ID() ?>"><?php
                                                                                                                                        if ('div' != $args['style']) { ?>
            <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
                                                                                                                                        } ?>
            <div class="row">
                <div class="col-sm-2 clearfix">
                    <div class="comment-gravatar vcard">
                        <?php
                        if ($args['avatar_size'] != 0) {
                            echo get_avatar($comment, $args['avatar_size']);
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-10">
                    <?php
                    // printf(__('<cite class="fn">%s</cite> <span class="says"></span>'), get_comment_author_link());
                    echo get_comment_author_link();
                    ?>
                    <time class="datetime" datetime="<?php echo get_comment_date() ?>"><?php echo get_comment_date() . ' at ' . get_comment_time() ?></time>
                    <!-- <a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)); ?>"></a> -->
                    <?php if (comments_open()) {
                    edit_comment_link(__('(Edit)'), '  ', '');
                    }
                    ?>
                    <?php
                    if ($comment->comment_approved == '0') { ?>
                        <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.'); ?></em><br />
                    <?php
                    } ?>
                    <?php comment_text(); ?>
                </div>
                <?php
                ?>
            </div>
            <!--/**end .row */ -->



            <div class="reply">
                <?php
                comment_reply_link(
                    array_merge(
                        $args,
                        array(
                            'add_below' => $add_below,
                            'depth'     => $depth,
                            'max_depth' => $args['max_depth']
                        )
                    )
                ); ?>
            </div><?php
                    if ('div' != $args['style']) : ?>
            </div><?php
                    endif;
                }
                    ?>