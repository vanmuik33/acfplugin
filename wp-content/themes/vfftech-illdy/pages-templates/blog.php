<?php
/*
Template Name: Blog
*/
?>
<?php get_header('post'); ?>
<?php
$args = array(
    'post_type' => array('post','blog-post'),
    'post_status' => 'publish',
    'post_per_page' => 3,
    'paged' => get_query_var('paged')
);
// query_posts('post_type=post&post_status=publish&posts_per_page=10&paged=' . get_query_var('paged')); 
query_posts($args);
?>
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <section id="blog">
                <?php
                if (have_posts()) :
                    while (have_posts()) :
                        the_post();
                ?>
                        <article id="<?php the_ID() ?>" <?php post_class('blog-post'); ?>>
                            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="blog-post-title"><?php the_title() ?></a>
                            <div class="blog-post-image">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail() ?></a>
                            </div>
                            <div class="blog-post-meta">
                                <span class="post-meta-author"> <i class="fa fa-user"></i><?php the_author() ?> </span>
                                <span class="post-meta-time"> <i class="fa fa-calendar"></i>
                                    <time class="datetime" datetime="<?php echo get_the_date('Y-m-d g:i A') ?>"><?php echo get_the_date('F d, Y') ?></time>
                                </span>
                                <span class="post-meta-categories">
                                    <i class="fa fa-folder-o"></i>
                                    <?php the_category('<a></a>') ?>
                                </span>
                                <span class="post-meta-comments">
                                    <i class="fa fa-comment-o">
                                    </i>
                                    <?php if (!comments_open()) echo 'Comments are off for this post';
                                    else comments_number() ?>
                                </span>
                            </div>
                            <div class="blog-post-entry">
                                <?php the_excerpt() ?>
                            </div>

                            <a href="<?php the_permalink() ?>" title="Read more" class="blog-post-button btn send">Read more</a>

                        </article>
                        <hr>
                    <?php
                    endwhile;
                    ?>
                    <div class="nav-previous alignleft"><?php previous_posts_link('Older posts'); ?></div>
                    <div class="nav-next alignright"><?php next_posts_link('Newer posts'); ?></div>
                <?php
                    wp_reset_query();
                endif;
                ?>
            </section>
        </div>
        <div class="col-sm-4">
            <?php get_sidebar() ?>
        </div>

    </div>
</div>
<?php get_footer(); ?>