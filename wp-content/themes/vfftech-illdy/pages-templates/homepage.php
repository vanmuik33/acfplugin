<?php 
/** Template Name: Homepage */
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/about') ?>
<?php get_template_part('template-parts/projects') ?>
<?php get_template_part('template-parts/testimonials') ?>
<?php get_template_part('template-parts/services') ?>
<?php get_template_part('template-parts/lastest-news') ?>
<?php get_template_part('template-parts/counter') ?>
<?php get_template_part('template-parts/team') ?>
<?php get_template_part('template-parts/contacts') ?> 


<?php get_footer(); ?>