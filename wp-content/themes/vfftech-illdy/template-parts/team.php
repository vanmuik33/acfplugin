<!-- ===================================================================== team =====================================================================-->
<section id="team" class="text-center" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/pattern.png' ?>);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Team</h3>
            </div>
            <div class="col-sm-10 col-sm-offset-1">
                <div class="section-description">
                    Meet the people that are going to take your business to the next level.
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php
            $team = get_field('team_group');
            if (have_rows('team_group')) :
                while (have_rows('team_group')) : the_row();
                    $total_row = count(get_sub_field('items'));
                    if (have_rows('items')) :
                        while (have_rows('items')) : the_row();
            ?>
                            <div class="col-sm-<?php echo 12/$total_row ?> col-sm-offset-0 col-xs-10 col-xs-offset-1">
                                <div class="clearfix">
                                    <div class="p-img">
                                        <img src="<?php echo get_sub_field('picture') ?>" class="img-circle" alt="">
                                    </div>
                                    <div class="p-info">
                                        <h6><?php echo get_sub_field('name') ?></h6>
                                        <p class="person-position" style="color: rgb(241, 139, 109);"><?php echo get_sub_field('position') ?></p>
                                        <p><?php echo get_sub_field('skill') ?></p>
                                        <ul class="person-content-social clearfix">
                                            <li><a href="<?php echo get_sub_field('fb') ?>" title="Facebook" target="_blank" rel="nofollow" style="border-color: rgb(241, 139, 109); color: rgb(241, 139, 109);"><i class="fa fa-facebook-f"></i></a></li>
                                            <li><a href="<?php echo get_sub_field('tw') ?>" title="Twitter" style="border-color: rgb(241, 139, 109); color: rgb(241, 139, 109);"><i class="fa fa-twitter" target="_blank" rel="nofollow"></i></a></li>
                                            <li><a href="<?php echo get_sub_field('lk') ?>" title="Twitter" style="border-color: rgb(241, 139, 109); color: rgb(241, 139, 109);"><i class="fa fa-linkedin" target="_blank" rel="nofollow"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
            <?php
                        endwhile;
                    endif;
                endwhile;
            else :
                // no rows found
                echo '<h3>No rows found</h3>';
            endif;
            ?>
        </div>
    </div>
</section>