<!-- ===================================================================== testimonials =====================================================================-->
<section id="testimonials" class="text-center" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/testiomnials-background.jpg' ?>);">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Testimonials</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="slick-slider">
                    <?php
                    if (have_rows('testimonials_group')) :
                        while (have_rows('testimonials_group')) : the_row();
                            $slider = get_sub_field('slider');
                            if ($slider) : foreach ($slider as $post) : setup_postdata($post);
                    ?>
                                    <div>
                                        <div class="testimonial-image">
                                            <img src="<?php the_post_thumbnail_url() ?>" class="img-circle" alt="<?php the_title() ?>">
                                        </div>
                                        <div class="testimonial-content">
                                            <blockquote><q><?php echo wp_strip_all_tags( get_the_content() );?></q></blockquote>
                                        </div>
                                        <div class="testimonial-meta">
                                            <h4><?php the_title() ?></h4>
                                        </div>
                                    </div>
                    <?php
                                endforeach;
                                wp_reset_query();
                            endif;
                        endwhile;
                    else :
                        // no rows found
                        echo '<h3>No rows found</h3>';
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>