<!-- ===================================================================== contact =====================================================================-->
<section id="contact" class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3>Contact Us</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-12 item">
                    <div class="left">
                        Address
                    </div>
                    <div class="right">
                        Street 221B Baker Street, London, UK
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 item">
                    <div class="left">
                        Customer Support
                    </div>
                    <div class="right text-left">
                        Email: <a href="mailto:contact@site.com" title="contact@site.com">contact@site.com</a> <br>
                        Phone: (555) 555-5555
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12 item social">
                    <div class="">
                        <ul>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
            $banner = get_field('contacts_group');
            if (have_rows('contacts_group') ):
                
                while (have_rows('contacts_group')) : the_row();
                    $form = get_sub_field('contact_form');
                    echo do_shortcode('[contact-form-7 id="'.$form[0].'"]');
                endwhile;
            else :
                // no rows found
                echo '<h3>No rows found</h3>';
            endif;
            ?>
        </div>
    </section>