<!-- ===================================================================== latest-news =====================================================================-->
<section id="latest-news">
        <div class="section-header text-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>Lastest News</h3>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="section-description">
                            <p>If you are interested in the latest articles in the industry, take a sneak peek at our blog. You have nothing to loose!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="<?php echo get_permalink(193) ?>" title="See blog" class="latest-news-button"><i class="fa fa-chevron-circle-right"></i>See blog </a>
        <div class="section-content">
            <div class="container">
                <div class="row">
                    











                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post" style="
                                ">
                            <div class="post-image" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/photo-1447687643809-e05fd462f350-360x213.jpg'?>);"></div>
                            <h5><a href="#" title="Hello world!" class="post-title">Hello world!</a></h5>
                            <div class="post-entry">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy… </div>
                            <!--/.post-entry-->
                            <a href="#" title="Read more" class="post-button"><i class="fa fa-chevron-circle-right"></i>Read more </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post" style="
                                ">
                            <div class="post-image" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/photo-1449168013943-3a15804bb41c-360x213.jpg'?>);"></div>
                            <h5><a href="#" title="Stunning One Page WordPress Theme For Masses" class="post-title">Stunning One Page WordPress Theme For Masses</a></h5>
                            <div class="post-entry">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy… </div>
                            <!--/.post-entry-->
                            <a href="#" title="Read more" class="post-button"><i class="fa fa-chevron-circle-right"></i>Read more </a>
                        </div>
                        <!--/.post-->
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="post" style="
                                ">
                            <div class="post-image" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/photo-1450101215322-bf5cd27642fc-360x213.jpg'?>);"></div>
                            <h5><a href="#" title="We Have Made Illdy Theme SEO Friendly" class="post-title">We Have Made Illdy Theme SEO Friendly</a></h5>
                            <div class="post-entry">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy… </div>
                            <!--/.post-entry-->
                            <a href="#" title="Read more" class="post-button"><i class="fa fa-chevron-circle-right"></i>Read more </a>
                        </div>
                        <!--/.post-->
                    </div>
                </div>
            </div>
        </div>
    </section>