<!-- ===================================================================== about =====================================================================-->
<section id="about">
    <div class="text-center container">
        <div class="row">
            <div class="col-sm-12">
                <h3>About</h3>
            </div>
            <div class="col-sm-10 col-sm-offset-1">
                <div class="section-description">
                    <!-- It is an amazng one-page theme with great features that offers an incredible experience. It is easy to install, make changes, adapt for your business. A modern design with clean lines and styling for a wide variety of content, exactly how a business design should be. You can add as many images as you want to the main header area and turn them into slider. -->
                    <?php 
                        // echo get_field('about_group', 'option')['description']
                        $about = get_field('about_group');
                        echo $about['description'];
                     ?>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <?php
                    if (have_rows('about_group')) :
                        while (have_rows('about_group')) : the_row();
                            $total_row = count(get_sub_field('items'));
                            if (have_rows('items')) :
                                while (have_rows('items')) : the_row();
                    ?>
                                    <div class="col-sm-<?php echo (12 / $total_row) ?> col-sm-offset-0 col-xs-10 col-xs-offset-1">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo get_sub_field('value') ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo get_sub_field('value') ?>%; background-color: <?php echo get_sub_field('color') ?>;">
                                                <span class="ui-progressbar-value-circle" style="background-color: <?php echo get_sub_field('color') ?>"></span>
                                                <span class="ui-progressbar-value-top" style="background-color: <?php echo get_sub_field('color') ?>">
                                                    <?php echo get_sub_field('value') ?>%
                                                    <span class="ui-progressbar-value-triangle" style="border-top-color: <?php echo get_sub_field('color') ?>">
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="skill-bottom text-left" style="color: <?php echo get_sub_field('color') ?>;"><span class="fa <?php echo get_sub_field('icon') ?>" aria-hidden="true"></span> <span><?php echo get_sub_field('title') ?></span></div>
                                    </div>
                    <?php
                                endwhile;
                            endif;
                        endwhile;
                    else :
                        // no rows found
                        echo '<h3>No rows found</h3>';
                    endif;
                    ?>
                    <!-- <div class="col-sm-4 col-sm-offset-0 col-xs-10 col-xs-offset-1">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width:60%; background-color: rgb(241, 139, 109);">
                                <span class="ui-progressbar-value-circle" style="background-color: #f18b6d"></span>
                                <span class="ui-progressbar-value-top" style="background-color: #f18b6d">
                                    60%
                                    <span class="ui-progressbar-value-triangle" style="border-top-color: #f18b6d">
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div class="skill-bottom text-left" style="color: rgb(241, 139, 109);"><span class="glyphicon glyphicon-font" aria-hidden="true"></span> <span>Typography</span></div>
                    </div> -->

                </div>
            </div>
        </div>
    </div>
</section>