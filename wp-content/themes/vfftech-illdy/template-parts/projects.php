<!-- ===================================================================== projects =====================================================================-->
<section id="projects" class="text-center" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/pattern.png' ?>);">
    <div class="container-fluid">
        <div class="row top-projects">
            <div class="col-sm-12">
                <h3 class="">Projects</h3>
                <div class="section-description"><?php echo get_field('project_group')['description'] ?></div>
            </div>
        </div>

        <div class="row">
            <?php
            if (have_rows('project_group')) :
                while (have_rows('project_group')) : the_row();
                    $total_row = count(get_sub_field('items'));
                    if (have_rows('items')) :
                        while (have_rows('items')) : the_row();
            ?>
                            <div class="col-sm-<?php echo (12/$total_row) ?> col-xs-6 no-padding project-image">
                                <a href="#">
                                    <img src="<?php echo get_sub_field('images') ?>" class="img-responsive project-image" alt="">
                                    <span class="overlay"></span>
                                </a>
                            </div>
            <?php
                        endwhile;
                    endif;
                endwhile;
            else :
                // no rows found
                echo '<h3>No rows found</h3>';
            endif;
            ?>
        </div>
    </div>
</section>