<!-- ===================================================================== counter =====================================================================-->
<section id="counter" class="text-center" style="background-image: url(<?php echo TPL_DIR_URI . '/assets/images/front-page-counter.jpg' ?>);">
    <div class="container">
        <div class="row counter">
            <?php
            if (have_rows('counter_group', 'option')) :
                while (have_rows('counter_group', 'option')) : the_row();
                    $total_row = count(get_sub_field('items'));
                    if (have_rows('items', 'option')) :
                        while (have_rows('items', 'option')) : the_row();
            ?>
                            <div class="col-sm-<?php if (12%$total_row == 0) echo (12/$total_row); else echo 15;?> col-xs-12">
                                <span class="counter-number" data-from="1" data-to="<?php echo get_sub_field('number') ?>" data-speed="2000" data-refresh-interval="100"><?php echo get_sub_field('number') ?></span>
                                <span class="section-description"><?php echo get_sub_field('title') ?></span>
                            </div>
            <?php
                        endwhile;
                    endif;
                endwhile;
            else :
                // no rows found
                echo '<h3>No rows found</h3>';
            endif;
            ?>
            <!-- <div class="col-sm-4 col-xs-12">
                <span class="counter-number" data-from="1" data-to="260" data-speed="2000" data-refresh-interval="100">260</span>
                <span class="section-description">PROJECTS</span>
            </div>
            <div class="col-sm-4 col-xs-12">
                <span class="counter-number" data-from="1" data-to="120" data-speed="2000" data-refresh-interval="100">120</span>
                <span class="section-description">CLIENTS</span>
            </div>
            <div class="col-sm-4 col-xs-12">
                <span class="counter-number" data-from="1" data-to="260" data-speed="2000" data-refresh-interval="100">260</span>
                <span class="section-description">COFFES</span>
            </div> -->
        </div>
    </div>
</section>