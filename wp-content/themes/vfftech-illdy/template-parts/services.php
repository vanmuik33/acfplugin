<!-- ===================================================================== Services =====================================================================-->
<section id="services" class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>Services</h3>
            </div>
            <div class="col-sm-10 col-sm-offset-1">
                <div class="section-description">order to help you grow your business, our carefully selected experts can advise you in in the following areas:</div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <?php
            if (have_rows('services_group', 'option')) :
                while (have_rows('services_group', 'option')) : the_row();
                    $total_row = count(get_sub_field('items'));
                    if (have_rows('items', 'option')) :
                        while (have_rows('items', 'option')) : the_row();
            ?>
                            <div class="col-sm-<?php echo (12/$total_row) ?>">
                                <div class="service">
                                    <div class="service-icon" style="color: <?php echo get_sub_field('color') ?>;"><i class="fa <?php echo get_sub_field('icon') ?>"></i></div>
                                    <div class="service-title" style="color: <?php echo get_sub_field('color') ?>;">
                                        <h5><?php echo get_sub_field('title') ?></h5>
                                    </div>
                                    <div class="service-entry"><?php echo get_sub_field('description') ?>
                                    </div>
                                </div>
                            </div>
            <?php
                        endwhile;
                    endif;
                endwhile;
            else :
                // no rows found
                echo '<h3>No rows found</h3>';
            endif;
            ?>
            <!-- <div class="col-sm-4">
                    <div class="service">
                        <div class="service-icon" style="color: rgb(241, 139, 109);"><i class="glyphicon glyphicon-pencil"></i></div>
                        <div class="service-title" style="color: rgb(241, 139, 109);">
                            <h5>Web Design</h5>
                        </div>
                        <div class="service-entry">Consectetur adipiscing elit. Praesent molestie urna hendrerit erat tincidunt tempus. Aliquam a leo risus. Fusce a metus non augue dapibus porttitor at in mauris. Pellentesque commodo...
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="service">
                        <div class="service-icon" style="color: rgb(241, 210, 4);"><i class="fa fa-code"></i></div>
                        <div class="service-title" style="color: rgb(241, 210, 4);">
                            <h5>WEB DEVELOPMENT</h5>
                        </div>
                        <div class="service-entry">Consectetur adipiscing elit. Praesent molestie urna hendrerit erat tincidunt tempus. Aliquam a leo risus. Fusce a metus non augue dapibus porttitor at in mauris. Pellentesque commodo...</div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="service">
                        <div class="service-icon" style="color: rgb(106, 77, 138);"><i class="fas fa-search"></i></div>
                        <div class="service-title" style="color: rgb(106, 77, 138);">
                            <h5>SEO Analisys</h5>
                        </div>
                        <div class="service-entry">Consectetur adipiscing elit. Praesent molestie urna hendrerit erat tincidunt tempus. Aliquam a leo risus. Fusce a metus non augue dapibus porttitor at in mauris. Pellentesque commodo...</div>
                    </div>
                </div> -->
        </div>
    </div>
</section>