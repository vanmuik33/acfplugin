<?php
define('TPL_DIR_URI', get_template_directory_uri());

# Khai báo hằng số TPL_DIR bằng đường dẫn đến thư mục theme
define('TPL_DIR', get_stylesheet_directory());

# Yêu cầu load file class-wp-bootstrap-navwalker.php
function register_navwalker()
{
    require_once TPL_DIR . '/inc/class-wp-bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');

function svh_enqueue_styles()
{
    wp_enqueue_style('bootstrap', TPL_DIR_URI . '/assets/vendor/bootstrap/css/bootstrap.min.css');
    if (is_front_page()) wp_enqueue_style('core', TPL_DIR_URI . '/style.css');
    else wp_enqueue_style('blog-style', TPL_DIR_URI . '/assets/css/blog-post.css');
    wp_enqueue_style('fonts', TPL_DIR_URI . '/assets/fonts/Poppins/stylesheet.css');
    wp_enqueue_style('slick', TPL_DIR_URI . '/assets/vendor/slick/slick.css');
    wp_enqueue_style('slick-theme', TPL_DIR_URI . '/assets/vendor/slick/slick-theme.css');
}
add_action('wp_enqueue_scripts', 'svh_enqueue_styles');

function svh_enqueue_scripts()
{
    wp_enqueue_script('popper', TPL_DIR_URI . '/assets/vendor/popper/popper.min.js', ['jquery']);
    wp_enqueue_script('bootstrap', TPL_DIR_URI . '/assets/vendor/bootstrap/js/bootstrap.min.js', ['jquery']);
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'svh_enqueue_scripts');

/**
  @ Them script vào hook footer
 **/
function svh_footer_enqueue_script()
{
    wp_enqueue_script('slick', TPL_DIR_URI . '/assets/vendor/slick/slick.min.js', ['jquery']);
    wp_enqueue_script('countTo', TPL_DIR_URI . '/assets/js/jquery.countTo.js', ['jquery']);
    if (is_front_page()) {
        wp_enqueue_script('main script', TPL_DIR_URI . '/assets/js/main.js', ['jquery']);
    }
}
add_action('wp_footer', 'svh_footer_enqueue_script');
/**
  @ Xoá margin-top: 32px !important;
 **/
// function remove_admin_login_header()
// {
//     remove_action('wp_head', '_admin_bar_bump_cb');
// }
// add_action('get_header', 'remove_admin_login_header');

/**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
 **/
if (!function_exists('theme_feature_setup')) {
    /*
     * Nếu chưa có hàm theme_feature_setup() thì sẽ tạo mới hàm đó
     */
    function theme_feature_setup()
    {
        /*
        * Thiết lập theme có thể dịch được
        */
        $language_folder = TPL_DIR_URI . '/languages';
        load_theme_textdomain('vfftech-illdy', $language_folder);
        /*
        * Thêm chức năng post thumbnail
        */
        add_theme_support('post-thumbnails');
        /*
        * Thêm chức năng title-tag để tự thêm <title>
        */
        add_theme_support('title-tag');
        /*
        * Thêm chức năng custom background
        */
        $default_background = array(
            'default-color' => '#e8e8e8',
        );
        add_theme_support('custom-background', $default_background);

        /*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
        add_theme_support(
            'html5',
            array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'script',
                'style',
            )
        );

        /*
        * Tạo menu cho theme
        */
        register_nav_menus([
            'main-menu' => __('Top menu')
        ]);

        /*
        * Tạo sidebar cho theme
        */
        $sidebar = array(
            'name' => __('Main Sidebar', 'vfftech-illdy'),
            'id' => 'main-sidebar',
            'description' => 'Main sidebar for vfftech-illdy theme',
            'class' => 'main-sidebar',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );
        register_sidebar($sidebar);
    }
    add_action('init', 'theme_feature_setup');
}


//Ghi đè main-query an toàn
function wpdocs_five_posts_on_homepage($query)
{
    if ($query->is_home() && $query->is_main_query()) {
        $query->set('posts_per_page', 10);
    }
}
add_action('pre_get_posts', 'wpdocs_five_posts_on_homepage');


/** Add custom post type: Blog-post */

function add_custom_post_type()
{

    /*
     * Biến $label để chứa các text liên quan đến tên hiển thị của Post Type trong Admin
     */
    $label = array(
        'name' => 'Custom posts', //Tên post type dạng số nhiều
        'singular_name' => 'Custom post' //Tên post type dạng số ít
    );

    /*
     * Biến $args là những tham số quan trọng trong Post Type
     */
    $args = array(
        'labels' => $label, //Gọi các label trong biến $label ở trên
        'description' => 'Post type test custom_post_type', //Mô tả của post type
        'supports' => array(
            'title',
            'editor',
            'excerpt',
            'author',
            'thumbnail',
            'comments',
            'trackbacks',
            'revisions',
            'custom-fields'
        ), //Các tính năng được hỗ trợ trong post type
        'taxonomies' => array('category', 'post_tag'), //Các taxonomy được phép sử dụng để phân loại nội dung
        'hierarchical' => false, //Cho phép phân cấp, nếu là false thì post type này giống như Post, true thì giống như Page
        'public' => true, //Kích hoạt post type
        'show_ui' => true, //Hiển thị khung quản trị như Post/Page
        'show_in_menu' => true, //Hiển thị trên Admin Menu (tay trái)
        'show_in_nav_menus' => true, //Hiển thị trong Appearance -> Menus
        'show_in_admin_bar' => true, //Hiển thị trên thanh Admin bar màu đen.
        'menu_position' => 5, //Thứ tự vị trí hiển thị trong menu (tay trái)
        'menu_icon' => '', //Đường dẫn tới icon sẽ hiển thị
        'can_export' => true, //Có thể export nội dung bằng Tools -> Export
        'has_archive' => true, //Cho phép lưu trữ (month, date, year)
        'exclude_from_search' => false, //Loại bỏ khỏi kết quả tìm kiếm
        'publicly_queryable' => true, //Hiển thị các tham số trong query, phải đặt true
        'capability_type' => 'post', //
        'rewrite' => true
    );

    register_post_type('blog-post', $args); //Tạo post type với slug tên là sanpham và các tham số trong biến $args ở trên
    flush_rewrite_rules();
}
/* Kích hoạt hàm tạo custom post type */
add_action('init', 'add_custom_post_type');

// Register Custom Post Type
function add_slider_post_type()
{

    $labels = array(
        'name'                  => _x('Sliders', 'Post Type General Name', 'vfftech-illdy'),
        'singular_name'         => _x('slider', 'Post Type Singular Name', 'vfftech-illdy'),
        'menu_name'             => __('Sliders', 'vfftech-illdy'),
        'name_admin_bar'        => __('Sliders', 'vfftech-illdy'),
        'archives'              => __('Item Archives', 'vfftech-illdy'),
        'attributes'            => __('Item Attributes', 'vfftech-illdy'),
        'parent_item_colon'     => __('Parent Item:', 'vfftech-illdy'),
        'all_items'             => __('All Items', 'vfftech-illdy'),
        'add_new_item'          => __('Add New Item', 'vfftech-illdy'),
        'add_new'               => __('Add New', 'vfftech-illdy'),
        'new_item'              => __('New Item', 'vfftech-illdy'),
        'edit_item'             => __('Edit Item', 'vfftech-illdy'),
        'update_item'           => __('Update Item', 'vfftech-illdy'),
        'view_item'             => __('View Item', 'vfftech-illdy'),
        'view_items'            => __('View Items', 'vfftech-illdy'),
        'search_items'          => __('Search Item', 'vfftech-illdy'),
        'not_found'             => __('Not found', 'vfftech-illdy'),
        'not_found_in_trash'    => __('Not found in Trash', 'vfftech-illdy'),
        'featured_image'        => __('Featured Image', 'vfftech-illdy'),
        'set_featured_image'    => __('Set featured image', 'vfftech-illdy'),
        'remove_featured_image' => __('Remove featured image', 'vfftech-illdy'),
        'use_featured_image'    => __('Use as featured image', 'vfftech-illdy'),
        'insert_into_item'      => __('Insert into item', 'vfftech-illdy'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'vfftech-illdy'),
        'items_list'            => __('Items list', 'vfftech-illdy'),
        'items_list_navigation' => __('Items list navigation', 'vfftech-illdy'),
        'filter_items_list'     => __('Filter items list', 'vfftech-illdy'),
    );
    $args = array(
        'label'                 => __('slider', 'vfftech-illdy'),
        'description'           => __('Show slider info', 'vfftech-illdy'),
        'labels'                => $labels,
        'supports'              => array('title', 'editor', 'thumbnail', 'revisions', 'custom-fields', 'post-formats'),
        'taxonomies'            => array('category', 'post_tag'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'rewrite' => true
    );
    register_post_type('slider', $args);
    flush_rewrite_rules();
}
add_action('init', 'add_slider_post_type', 0);

add_filter('pre_get_posts', 'lay_custom_post_type');
function lay_custom_post_type($query)
{
    if (is_home() && $query->is_main_query())
        $query->set('post_type', array('post', 'blog-post'));
    return $query;
}

if (!function_exists('display_menu')) {
    function display_menu($slug)
    {
        $menu = array(
            'theme_location' => $slug,
            'container' => 'div',
            'container_id' => 'myNavbar',
            'container_class' => 'collapse navbar-collapse',
            'menu_class'        => 'text-center nav navbar-nav navbar-right'
        );
        wp_nav_menu($menu);
    }
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title'     => 'Theme options', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Theme options', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'    => false
    ));
}

// Remove p tags from the_content
// remove_filter ('the_content', 'wpautop');
// // Remove p tags from the_exceprt
// remove_filter ('the_exceprt', 'wpautop');
// // Remove p tags from category description
// remove_filter('term_description','wpautop');


require_once get_parent_theme_file_path('/custom-comments.php');