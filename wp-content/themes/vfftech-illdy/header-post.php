<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" />
    <meta charset="<?php bloginfo('charset'); ?>" />
    <?php wp_head();
    $blog = get_field('blog_group', 'option');
    $homepage = get_field('banner_group', 'option');
    ?>
</head>

<body <?php body_class('blog'); ?>>
    <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
    <!-- page loading  -->
    <div class="pace-overlay"></div>
    <!-- ===================================================================== header =====================================================================-->
    <header id="header" style="background-image: url(<?php echo $blog['background'] ?>);">
        <div class="top-header">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="<?php echo $homepage['logo']  ?>" alt=""></a>
                    </div>
                    <?php display_menu('main-menu') ?>
                </div>
            </nav>
        </div>
        <div class="bottom-header blog">
            <div class="container">
                <div class="row">
                    <?php if (is_single()) : ?>
                        <div class="col-sm-12">
                            <h1><?php the_title() ?></h1>
                        </div>
                    <?php else : ?>
                        <div class="col-sm-12">
                            <h2><?php echo $blog['title'] ?></h2>
                        </div>
                        <!--/.col-sm-12-->
                        <div class="col-sm-8 col-sm-offset-2">
                            <p><?php echo $blog['description'] ?></p>
                        </div>
                        <!--/.col-sm-8.col-sm-offset-2-->
                    <?php endif; ?>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </div>
    </header>